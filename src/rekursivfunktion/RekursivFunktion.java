/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekursivfunktion;

import java.util.*;

/**
 *
 * @author Fredrik
 */
public class RekursivFunktion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Skapa en int array och tilldela den lite värden
        int[] tal = {1,2,3,4,5,6,7,8,9};        
        
        // Kalla på vår rekursiva funktion
        bakovant(tal, tal.length-1);
        
    }
    
    public static void bakovant( int[] arr, int length )
    {
        // Kör så länge vi inte har kommit till array index 0
        if( length >= 0 )
        {
            // Skriv ut arrayen (Bakofram) 
            System.out.println( arr[length] );
            // Återkalla denna funktion och minska array indexet med 1
            bakovant( arr, length-1 );
        }
    }
    
}
